#!/bin/bash

# ~~~ >> partially smashed together by ayunami2000 and heavily pulverized and turned into sedimentary code by sdft
if [ -z $(command -v javac) ]; then
    echo "You're missing java"
    exit 1
fi
if [ -z $(command -v wget) ]; then
    echo "You're missing wget"
    exit 1
fi
if [ -z $(command -v git) ]; then
    echo "You're missing git"
    exit 1
fi

JAVA11="$(command -v javac)"
JAVA11="${JAVA11%?}"
BASEDIR=$($1 || pwd)

if [ ! -f falconcraftx ]; then
    git clone https://gitlab.com/sdft/falconcraft-x --depth 1 $($BASEDIR || falconcraftx)
fi

rm buildconf.json
mkdir tmp
if [ ! -f tmp/mcp918.zip ]; then
    wget -O tmp/mcp918.zip http://www.modcoderpack.com/files/mcp918.zip
fi
if [ ! -f tmp/1.8.8.jar ]; then
    wget -O tmp/1.8.8.jar https://launcher.mojang.com/v1/objects/0983f08be6a4e624f5d85689d1aca869ed99c738/client.jar
fi
if [ ! -f tmp/1.8.json ]; then
    wget -O tmp/1.8.json https://launchermeta.mojang.com/v1/packages/f6ad102bcaa53b1a58358f16e376d548d44933ec/1.8.json
fi
buildconf=$(cat << EOF
{
	"repositoryFolder": "BASEDIR/falconcraftx",
	"modCoderPack": "tmp/mcp918.zip",
	"minecraftJar": "tmp/1.8.8.jar",
	"assetsIndex": "tmp/1.8.json",
	"outputDirectory": "tmp/output",
	"temporaryDirectory": "tmp/##FALCON.TEMP##",
	"ffmpeg": "ffmpeg",
	"productionIndex": "BASEDIR/falconcraftx/buildtools/production-index-ext.html",
	"productionFavicon": "BASEDIR/falconcraftx/buildtools/production-favicon.png",
	"addScripts": [
		"eaglercraft_opts.js"
	],
	"removeScripts": [],
	"injectInOffline": [],
	"mavenURL": "https://repo1.maven.org/maven2/",
	"mavenLocal": "tmp/teavm",
	"generateOfflineDownload": true,
	"offlineDownloadTemplate": "BASEDIR/falconcraftx/sources/setup/workspace_template/javascript/OfflineDownloadTemplate.txt",
	"keepTemporaryFiles": false,
	"writeSourceMap": true,
	"minifying": true
}
EOF
)
echo $buildconf > buildconf_template.json
sed "s#BASEDIR#$BASEDIR#" buildconf_template.json > buildconf.json
"$JAVA11" -Xmx512M -cp "falconcraftx/buildtools/BuildTools.jar" net.lax1dude.eaglercraft.v1_8.buildtools.gui.headless.CompileLatestClientHeadless -y buildconf.json
retVal=$?
rm -rf tmp/##FALCON.TEMP##
rm -rf tmp/teavm
mkdir web
if [ $retVal -eq 0 ]; then
    cp -r tmp/output/* web/
fi
#rm -rf tmp/output #commented out because i want to be safe and not have to recompile again