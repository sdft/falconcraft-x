# Contributing
If you want to contribute, just request developer access to the repo and make your changes. Easy as that.
## Note on changes that make it not build with build.sh
If you have any changes of that type, then just make a note of that in the commit message.
