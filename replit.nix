{ pkgs }: {
    deps = [
        pkgs.wget
        pkgs.adoptopenjdk-openj9-bin-11
        pkgs.cowsay
    ];
}